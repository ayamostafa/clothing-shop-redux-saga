import React from 'react';
import StripeCheckout from 'react-stripe-checkout';
import axios from 'axios';

const StripeCheckoutButton = ({price}) => {
    const priceForStripe = price * 100;
    const publishableKey = 'pk_test_51JTnCZF5a2cDFLQdIOHOLhDaoFq5iD05Q8kO0zEE0Xjo7gbVBNjVBIe64OmFeTWEP2vLgPG4SouRC837zuwqAL9h00SKzxxMsv';
    const onToken = token => {
        console.log(token);
        axios({
            url: '/payment',
            method: 'post',
            data: {
                amount: priceForStripe,
                token
            }
        })
            .then(response => {
                alert('succesful payment');
            })
            .catch(error => {
                console.log('Payment Error: ', error);
                alert(
                    'There was an issue with your payment! Please make sure to use the provided credit card data'
                );
            });
    };

    return (
        <StripeCheckout
            label='Pay Now'
            name='Aya Clothing Shope'
            billingAddress
            shippingAddress
            image='https://us.123rf.com/450wm/dariachekman/dariachekman1910/dariachekman191000016/131495659-orange-store-icon-shop-icon-with-flat-shadow-on-a-blue-background-flat-design-vector-illustration.jpg?ver=6'
            description={`Your total is $${price}`}
            amount={priceForStripe}
            panelLabel='Pay Now'
            token={onToken}
            stripeKey={publishableKey}
        />
    );
};

export default StripeCheckoutButton;