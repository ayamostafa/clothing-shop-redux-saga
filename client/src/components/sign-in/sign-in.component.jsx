import React, {useState} from 'react';
import {auth,signInWithGoogle} from '../../firebase/firebase.utils';
import FormInput from '../form-input/form-input.component';
import CustomButton from '../custom-button/custom-button.component';

import './sign-in.styles.scss';

function SignIn (){
  const [email,setEmail] = useState('');
  const [password,setPassword] = useState('');

  const handleSubmit = async event => {
    event.preventDefault();
    try {
      await auth.signInWithEmailAndPassword(email, password);
      setEmail('');
      setPassword('');
      alert('Successfully Logged in')
    } catch (error) {
      alert(error);
      console.log(error);
    }
  };

  const handleChange = event => {
    const { value, name } = event.target;
    if(name === 'email') setEmail(value);
    if(name === 'password') setPassword(value);
  };

    return (
      <div className='sign-in'>
        <h2>I already have an account</h2>
        <span>Sign in with your email and password</span>

        <form onSubmit={handleSubmit}>
          <FormInput
            name='email'
            type='email'
            handleChange={handleChange}
            value={email}
            label='email'
            required
          />
          <FormInput
            name='password'
            type='password'
            value={password}
            handleChange={handleChange}
            label='password'
            required
          />
          <div className='btn-actions'>
          <CustomButton type='submit'> Sign in </CustomButton>
          <CustomButton  type="button"  onClick={signInWithGoogle} isGoogleSignIn> Sign in With Google </CustomButton>
          </div>
        </form>
      </div>
    );
}

export default SignIn;
